package co.seespace.navigation.view;

import android.os.Bundle;

import co.seespace.navigation.R;
import co.seespace.navigation.viewmodel.FirstViewModel;
import inair.app.IAFragment;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/7/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class FirstLayer extends IAFragment {
  @Override
  public void onInitialize(Bundle bundle) {
    setAndBindRootContentView(R.layout.first_layer, new FirstViewModel(this));
  }
}
