package co.seespace.navigation.view;

import android.os.Bundle;

import inair.app.IAFragment;
import inair.view.UILayeredViewItem;
import inair.view.UITextView;
import inair.view.UIViewGroup;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/7/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class ThirdLayer extends IAFragment {
  @Override
  public void onInitialize(Bundle bundle) {
    UILayeredViewItem root = new UILayeredViewItem(this);
    root.setTitle("third");
    root.setShouldOpen(true);

    UIViewGroup contentView = new UIViewGroup(this, false , true);

    UITextView textView = new UITextView(this);
    textView.setFontSize(30);
    textView.setWidth(300f);
    textView.setTextAlignment(UITextView.TextAlignment.CENTER);
    textView.setText("Programmatically Views");
    contentView.addView(textView);

    root.setContentView(contentView);

    setRootContentView(root);
  }
}
