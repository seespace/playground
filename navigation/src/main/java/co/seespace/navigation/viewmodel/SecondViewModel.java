package co.seespace.navigation.viewmodel;

import android.graphics.drawable.Drawable;

import co.seespace.navigation.R;
import inair.app.IAContext;
import inair.data.LayeredItemViewData;
import inair.data.ViewModel;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/7/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class SecondViewModel extends ViewModel implements LayeredItemViewData {

  private String text;

  public String getText() {
    return text;
  }

  public void setText(String text) {
    this.text = text;
    notifyPropertyChanged("text");
  }

  private int image;

  public Drawable getImage() {
    return getResources().getDrawable(image);
  }

  public void setImage(int image) {
    this.image = image;
  }

  public SecondViewModel(IAContext context) {
    super(context);
    setText("InAiR - This is the second layer screen with binding content");
    setImage(R.drawable.ic_logo2d3d);
  }

  @Override
  public CharSequence getTitle() {
    return "second";
  }

  @Override
  public boolean getShouldOpen() {
    return true;
  }
}
