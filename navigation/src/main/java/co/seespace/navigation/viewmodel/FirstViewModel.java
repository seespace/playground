package co.seespace.navigation.viewmodel;

import inair.app.IAContext;
import inair.data.LayeredItemViewData;
import inair.data.ViewModel;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/7/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class FirstViewModel extends ViewModel implements LayeredItemViewData {
  public FirstViewModel(IAContext context) {
    super(context);
  }

  @Override
  public CharSequence getTitle() {
    return "first";
  }

  @Override
  public boolean getShouldOpen() {
    return true;
  }
}
