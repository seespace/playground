package co.seespace.customui;

import android.os.Bundle;

import co.seespace.customui.view.TVGuideLayout;
import inair.app.IAActivity;
import inair.view.UILayeredNavigationView;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/22/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class RootLayout extends IAActivity {
  @Override
  public void onInitialize(Bundle bundle) {
    UILayeredNavigationView rootView = new UILayeredNavigationView(this);
    setRootContentView(rootView);

    addChildLayout(new TVGuideLayout());
  }
}
