package co.seespace.customui.view;

import android.os.Bundle;

import co.seespace.customui.R;
import co.seespace.customui.viewmodel.TVGuideViewModel;
import inair.app.IAFragment;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/22/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class TVGuideLayout extends IAFragment {
  @Override
  public void onInitialize(Bundle bundle) {
    setAndBindRootContentView(R.layout.guide_layout, new TVGuideViewModel(this));
  }
}
