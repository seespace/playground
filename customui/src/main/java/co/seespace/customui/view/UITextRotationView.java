package co.seespace.customui.view;

import android.util.AttributeSet;

import inair.app.IAContext;
import inair.view.UITextView;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/22/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class UITextRotationView extends UITextView {
  public UITextRotationView(IAContext context, AttributeSet attrs) {
    super(context, attrs);
    setAnchorPoint(0f, 1f, 0f);
    setRotation2D(-90f);
  }
}
