package co.seespace.customui.viewmodel;

import inair.app.IAContext;
import inair.collection.ObservableCollection;
import inair.data.LayeredItemViewData;
import inair.data.ViewModel;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/22/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class TVGuideViewModel extends ViewModel implements LayeredItemViewData {
  public final ObservableCollection<TVGuideItemViewModel> items = new ObservableCollection<>();

  private CharSequence mHeaderText;

  public CharSequence getHeaderText() {
    return "now";
  }

  public TVGuideViewModel(IAContext context) {
    super(context);

    for (int i = 0; i < 50; i++) {
      items.add(new TVGuideItemViewModel(context));
    }
  }

  @Override
  public CharSequence getTitle() {
    return "tv guide";
  }

  @Override
  public boolean getShouldOpen() {
    return true;
  }
}
