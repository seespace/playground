package co.seespace.customui.viewmodel;

import inair.app.IAContext;
import inair.data.ItemViewData;
import inair.data.ViewModel;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * Created by linh on 9/22/15.
 * <p/>
 * <p>Copyright (c) 2015 SeeSpace.co. All rights reserved.</p>
 */
public class TVGuideItemViewModel extends ViewModel implements ItemViewData {
  public TVGuideItemViewModel(IAContext context) {
    super(context);
  }

  @Override
  public void setUp() {

  }

  @Override
  public void tearDown() {

  }

  @Override
  public CharSequence getTag() {
    return "guide";
  }
}
