package co.seespace.presenting.viewmodel;

import inair.app.IAContext;
import inair.data.ItemViewData;
import inair.data.ViewModel;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class ItemVM extends ViewModel implements ItemViewData {

  private String title;
  private String shortDesc;

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
    notifyPropertyChanged("title");
  }

  public String getShortDesc() {
    return shortDesc;
  }

  public void setShortDesc(String shortDesc) {
    this.shortDesc = shortDesc;
    notifyPropertyChanged("shortDesc");
  }

  public ItemVM(IAContext context, String title, String shortDesc) {
    super(context);

    setTitle(title);
    setShortDesc(shortDesc);
  }

  @Override
  public void setUp() {

  }

  @Override
  public void tearDown() {

  }

  @Override
  public CharSequence getTag() {
    return "itemTag";
  }
}
