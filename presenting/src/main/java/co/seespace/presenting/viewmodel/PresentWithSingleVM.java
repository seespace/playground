package co.seespace.presenting.viewmodel;

import inair.app.IAContext;
import inair.data.LayeredItemViewData;
import inair.data.ViewModel;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class PresentWithSingleVM extends ViewModel implements LayeredItemViewData {
  public PresentWithSingleVM(IAContext context) {
    super(context);
  }

  @Override
  public CharSequence getTitle() {
    return "Present 1";
  }

  @Override
  public boolean getShouldOpen() {
    return true;
  }
}
