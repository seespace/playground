package co.seespace.presenting.viewmodel;

import inair.app.IAContext;
import inair.data.ViewModel;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class PresentedDetailItemVM extends ViewModel {
  private String detailTitle;
  private String detailDesc;

  public String getDetailTitle() {
    return detailTitle;
  }

  public void setDetailTitle(String detailTitle) {
    this.detailTitle = detailTitle;
    notifyPropertyChanged("detailTitle");
  }

  public String getDetailDesc() {
    return detailDesc;
  }

  public void setDetailDesc(String detailDesc) {
    this.detailDesc = detailDesc;
    notifyPropertyChanged("detailDesc");
  }

  public PresentedDetailItemVM(IAContext context, String title) {
    super(context);

    setDetailTitle(title.toUpperCase());
    setDetailDesc(SAMPLE_DETAIL);
  }

  private String SAMPLE_DETAIL = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris nunc erat, mattis vel nulla nec, venenatis facilisis elit. Nulla finibus arcu non dolor elementum, at vehicula lacus consectetur. Morbi pharetra, velit ut pulvinar convallis, nunc nisi vestibulum metus, sit amet fringilla sapien lectus non enim. Nulla dignissim varius nulla ac dictum. Maecenas tincidunt accumsan odio ultrices tempor. Sed posuere eget elit vel pharetra. Pellentesque vitae neque eu tellus vehicula gravida ut id risus.\n" +
    "\n" +
    "Cras vitae cursus nunc, vel eleifend lectus. Phasellus tincidunt nunc id tempus convallis. Vivamus eleifend dolor erat. Vestibulum lacinia ante porttitor erat tempor, vitae pulvinar orci porttitor. Integer mollis nec orci et facilisis. In at tincidunt felis, non hendrerit odio. Phasellus facilisis suscipit ante in faucibus. Vivamus lacinia eros vel ante faucibus, dictum porttitor libero tristique. Maecenas iaculis dui odio, et lobortis leo pellentesque vitae. Fusce id facilisis enim. Maecenas facilisis arcu et sem euismod ornare. Ut ultricies, leo nec vestibulum viverra, nibh velit tincidunt nibh, quis viverra justo risus eu odio. Nam velit metus, rutrum non feugiat a, tempus ac neque.\n" +
    "\n";
}
