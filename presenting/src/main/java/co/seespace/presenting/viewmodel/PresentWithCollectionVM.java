package co.seespace.presenting.viewmodel;

import inair.app.IAContext;
import inair.collection.ObservableCollection;
import inair.data.LayeredItemViewData;
import inair.data.ViewModel;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class PresentWithCollectionVM extends ViewModel implements LayeredItemViewData {
  public PresentWithCollectionVM(IAContext context) {
    super(context);

    listItems.add(new ItemVM(context, "Sample Title 1", "Short Description 1"));
    listItems.add(new ItemVM(context, "Sample Title 2", "Short Description 2"));
    listItems.add(new ItemVM(context, "Sample Title 3", "Short Description 3"));
    listItems.add(new ItemVM(context, "Sample Title 4", "Short Description 4"));
    listItems.add(new ItemVM(context, "Sample Title 5", "Short Description 5"));
  }

  @Override
  public CharSequence getTitle() {
    return "Present 2";
  }

  @Override
  public boolean getShouldOpen() {
    return true;
  }

  public ObservableCollection<ItemVM> listItems = new ObservableCollection<>();
}
