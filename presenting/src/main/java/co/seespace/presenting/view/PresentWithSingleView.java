package co.seespace.presenting.view;

import android.os.Bundle;

import co.seespace.presenting.R;
import co.seespace.presenting.viewmodel.PresentWithSingleVM;
import inair.app.IAFragment;
import inair.event.Event;
import inair.input.TouchEventArgs;
import inair.view.UIView;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class PresentWithSingleView extends IAFragment {
  @Override
  public void onInitialize(Bundle bundle) {
    setAndBindRootContentView(R.layout.single_view_layer, new PresentWithSingleVM(this));

    addViewEventListener(UIView.DoubleTapEvent, doubleTapToPresentListener);
  }

  Event.Listener<TouchEventArgs> doubleTapToPresentListener = new Event.Listener<TouchEventArgs>() {
    @Override
    public void onTrigger(Object o, TouchEventArgs touchEventArgs) {
      present(new PresentedSingleContentView());
    }
  };
}
