package co.seespace.presenting.view;

import android.os.Bundle;

import co.seespace.presenting.R;
import co.seespace.presenting.viewmodel.ItemVM;
import co.seespace.presenting.viewmodel.PresentedDetailItemVM;
import inair.app.IAFragment;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class PresentedCollectionContentView extends IAFragment {
  @Override
  public void onInitialize(Bundle bundle) {
    setRootContentView(R.layout.item_detail_layout);

    ItemVM itemViewModel = ((ItemVM) getDataContext());
    PresentedDetailItemVM presentedDetailItemVM = new PresentedDetailItemVM(this, itemViewModel.getTitle());

    setDataContext(presentedDetailItemVM);
  }
}
