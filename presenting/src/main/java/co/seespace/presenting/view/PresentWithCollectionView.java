package co.seespace.presenting.view;

import android.os.Bundle;

import co.seespace.presenting.R;
import co.seespace.presenting.viewmodel.ItemVM;
import co.seespace.presenting.viewmodel.PresentWithCollectionVM;
import inair.app.IAFragment;
import inair.event.Event;
import inair.input.TouchEventArgs;
import inair.view.UIListView;
import inair.view.UIListViewItem;
import inair.view.UIView;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class PresentWithCollectionView extends IAFragment {
  @Override
  public void onInitialize(Bundle bundle) {
    setAndBindRootContentView(R.layout.collection_view_layout, new PresentWithCollectionVM(this));

    addViewEventListener(UIView.DoubleTapEvent, doubleTapListener);

    listItem = ((UIListView) findUIViewById(R.id.listId));
  }

  UIListView listItem;

  private Event.Listener<TouchEventArgs> doubleTapListener = new Event.Listener<TouchEventArgs>() {
    @Override
    public void onTrigger(Object o, TouchEventArgs touchEventArgs) {
      UIListViewItem highlightItem = listItem.getHighlightItem();
      if (highlightItem != null) {
        ItemVM highlightVM = ((ItemVM) highlightItem.getDataContext());

        PresentedCollectionContentView itemDetailView = new PresentedCollectionContentView();
        itemDetailView.setDataContext(highlightVM);

        present(itemDetailView);
      }
    }
  };
}
