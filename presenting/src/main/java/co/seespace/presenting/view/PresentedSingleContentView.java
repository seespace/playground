package co.seespace.presenting.view;

import android.os.Bundle;
import android.os.Handler;

import co.seespace.presenting.R;
import inair.app.IAFragment;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class PresentedSingleContentView extends IAFragment {
  @Override
  public void onInitialize(Bundle bundle) {
    setRootContentView(R.layout.single_view_content_layout);
  }

  @Override
  public void onRootViewDidAppear() {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        dismiss();
      }
    }, 10000);
  }
}
