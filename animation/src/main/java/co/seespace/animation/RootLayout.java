package co.seespace.animation;

import android.os.Bundle;

import inair.app.IAActivity;
import inair.view.UIAnimation;
import inair.view.UILayeredNavigationView;

/**
 * <p>
 * Note this class is currently under early design and development.
 * The API will likely change in later updates of the compatibility library,
 * requiring changes to the source code of apps when they are compiled against the newer version.
 * </p>
 * <p/>
 * <p>Copyright (c) 2014 SeeSpace.co. All rights reserved.</p>
 */
public class RootLayout extends IAActivity {
  @Override
  public void onInitialize(Bundle bundle) {
    UILayeredNavigationView rootView = new UILayeredNavigationView(this);
    setRootContentView(rootView);
  }
}
